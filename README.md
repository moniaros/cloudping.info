# cloudping.info Website Source

<https://cloudping.info/>

The website is hosted on [GitLab Pages](https://pages.gitlab.io/).

# How to Change the Website

1. Make a gitlab.com account
2. Fork this repository
3. Commit changes to your fork
4. Create a Pull Request (PR) to merge your changes into this repository.
5. Email a signed copy of [`cla-email-template.txt`](https://gitlab.com/leonhard-llc/cloudping.info/-/blob/master/cla-email-template.txt)
6. Respond to comments on your PR
7. When we merge your changes, they go live immediately.

Thanks for your contribution!

# TO DO
- Add other cloud providers.
- Update to modern JavaScript.  Stop using jQuery.
